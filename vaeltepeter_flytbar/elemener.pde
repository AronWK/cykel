void forhjul(){
  pushMatrix();
  
  translate(plx+250, ply+150);
  rotate(frameCount / frCnt);
  //forhjul
  noFill();
  strokeWeight(7);
  ellipse(0,0, 300,300);
  //eger
  //for
  strokeWeight(1);
  line(0,0, 150, 0);
  line(0,0, -150,0);
  line(0,0, 0,150);
  line(0,0, 0,-150);
  line(0,0, 110,100);
  line(0,0, 110,-100);
  line(0,0, -110,100);
  line(0,0, -110,-100);
  popMatrix();
}
void fod(){
  
  strokeWeight(4);
  pushMatrix();
  translate(30,-20);
  rotate(frameCount / -frCnt );
  line(+5,0,-5,0);
  popMatrix();
  pushMatrix();
  translate(-30, 20);
  rotate(frameCount / -frCnt );
  line(-5,0,+5,0);
  popMatrix();
}
void pedaler(){
  pushMatrix();
  translate(plx+250, ply+150);
  rotate(frameCount / frCnt);
  //pedaler
  strokeWeight(2);
  line(0,0,30,-20);
  line(0,0,-30,+20);
  fod();
  popMatrix();
  
}
void baghjul(){
  pushMatrix();
  translate(plx-100, ply+250);
  rotate( frameCount / (frCnt / 2) );
  //baghjul
  strokeWeight(5);
  ellipse(0,0, 100,100);
  strokeWeight(1);
  //eger
  //bag
  line(0,0, 0,-50);
  line(0,0, 0,+50);
  line(0,0, -50,0);
  line(0,0, +50,0);
  popMatrix();
}
