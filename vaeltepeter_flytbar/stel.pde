void stel(){
  noFill();
 
  //forgaffel
  strokeWeight(2);
  line(plx+250,ply+150, plx+199, ply-70);
  
  //tværstang
  arc(plx+245, ply+140, 430, 430, 3.2, 4.5);//øverst
  line(plx+30,ply+127, plx-100,ply+250); //nederst
  
  //styr
  line(plx+199,ply-70,plx+190,ply-100);
  strokeWeight(5);
  line(plx+190,ply-100,plx+175,ply-100);
  strokeWeight(1);
  
  //saddel
  fill(139, 69, 19);
  noStroke();
  arc(plx+130,ply-40, 60,40, 3,5.2);
  noFill();
  strokeWeight(1);
  
}
