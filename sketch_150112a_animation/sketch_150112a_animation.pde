
int a = 1000;
int cyk = 0;
void setup(){
  size(a,a-200);
  background(255);

}

void draw(){
  println(mouseX, mouseY);
  
  for (int i = 0; i < 601; i = i+1) {
    cyk++;
    bike(cyk);
    println("Draw bike, i = " + i + " cyk = " + cyk);
    
  }
  
}


void bike(int cyk){
  background(255);
  noFill();
  strokeWeight(5);
  ellipseMode(CORNER);
  ellipse(cyk, cyk-100, cyk-300,cyk-300); //forhjul
  
  ellipse(cyk-250, cyk+100, cyk-500,cyk-500); //baghjul
  
  strokeWeight(1);
  //forgaffel
  line(cyk+150,cyk+50, cyk+100, cyk-170); 
  //stang øverst
  arc(cyk-70, cyk-175, cyk-170,cyk-170, 3.2, 4.5 ); 
  // stang nederst
  line(cyk-70,cyk+25, cyk-200,cyk+150); 
  
  // styr
  line(cyk+100,cyk-170, cyk+90,cyk-200); 
  strokeWeight(5);
  line(cyk+90,cyk-200, cyk+75,cyk-200);
  strokeWeight(1);
  
  // saddel
  fill(0);
  arc(cyk,cyk-160, 60,40, 3, 5.2);
  
  //pedal
  line(cyk+150,cyk+50, cyk+180,cyk+30);
  line(cyk+150,cyk+50, cyk+120, cyk+70);
  strokeWeight(3);
  line(cyk+185,cyk+30, cyk+175,cyk+30);
  line(cyk+125,cyk+70, cyk+115,cyk+70);
//noLoop();
}
